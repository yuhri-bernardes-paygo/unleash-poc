# unleash-poc

FIXME: my new application.

## Installation

Download from https://github.com/yuhribernardes/unleash-poc.

## Usage

Run the project directly:

    $ clojure -m yuhribernardes.unleash-poc

Build an uberjar:

    $ clojure -A:uberjar

Run that uberjar:

    $ java -jar unleash-poc.jar

## License

Copyright © 2020 Yuhri

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
