(ns unleash.core
  (:import no.finn.unleash.util.UnleashConfig
           no.finn.unleash.DefaultUnleash
           no.finn.unleash.strategy.DefaultStrategy))

(defn map->UnleashConfig [{:keys [url
                                  app-name
                                  instance-id] :as opts}]
  (-> (UnleashConfig/builder)
      (.appName app-name)
      (.instanceId instance-id)
      (.unleashAPI url)
      (.build)))

(defn new-client
  [opts]
  (let [configs (map->UnleashConfig opts)
        unleash-instance (DefaultUnleash. configs nil)]
    unleash-instance))

(defn enabled?
  ([client flag-name]
   (.isEnabled client flag-name))
  ([client flag-name default-value]
   (.isEnabled client flag-name default-value)))

(comment
  (def client (new-client {:url  "http://localhost:4242/api/"
                           :app-name "my-first-server"
                           :instance-id "my-server-id-1"}))

  (enabled? client "get-toggle2" false)



  )
