(ns yuhribernardes.server
  (:gen-class)
  (:require [io.pedestal.http :as http]
            [io.pedestal.http.route :as router]
            [cheshire.core :as json]
            [unleash.core :as unleash]))

(def client (unleash/new-client {:url  "http://localhost:4242/api/"
                         :app-name "my-first-server"
                         :instance-id "my-server-id-1"}))

(def hello-interceptor
  {:name ::hello
   :enter (fn [context]
            (let [res-body (if (unleash/enabled? client "get-toggle")
                             {:msg "Salve queridão"}
                             {:msg "Tranquilo meu bacano?"})]
              (assoc context :response {:status 200 :body (json/encode res-body)})))})

(def routes (router/expand-routes #{["/" :get hello-interceptor]}))

(def service-map {::http/type :jetty
                  ::http/routes routes
                  ::http/port 3000
                  ::http/join? false})

(defonce server (atom nil))

(defn stop-some-server [server-atom]
  (when @server-atom (swap! server-atom http/stop))
  server-atom)

(defn start-server [config server-atom]
  (reset! server-atom (-> config
                     http/create-server
                     http/start)))

(defn -main
  "I don't do a whole lot ... yet."
  [& _]
  (->> server
      stop-some-server
      (start-server service-map)))

(comment
  (-main)

  (stop-some-server server)
  )
